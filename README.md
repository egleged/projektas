**MEDŽIAI**


*Visi medziai yra aplanke "Sklandytuvo prototipas\Assets\Medziai"
Visi medziu `Materials` yra alanke "Sklandytuvo prototipas\Materials"
Visos tekstūros ir kt. yra aplanke "Sklandytuvo prototipas\Assets" *

---

## Nustatymai

Nuotrauka, pavadinimu "settings", yra preliminarūs nustatymai kuriant medį, geriausiai tinkantys kuriant eglę, tačiau šiek tiek pakeitus rotation nustatymus eis sukusrti ir kitokį.

---

## Miškas
 
 Miškas yra sukurtas, pridetos visos detales, daugiau koreguoti nebereikia, galbūt (matysime eigoje programavimo) reikės tik jį prailginti.
 Taip pat reikėtų sukurti medžių judėjimo animaciją. Šiuo metu bandoma išsiaiškinti, kaip lengviau būtų galima ją sukurti.
 
 ---

## Kodas
 
 Visas programos kodas yra aplanke Sklandytuvo prototipas/Assets/Scripts.
